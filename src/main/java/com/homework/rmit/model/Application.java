package com.homework.rmit.model;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Application{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String appGroup;
    private String appType;
    private String description;
    private long appCost;
    private Date lastModified;

    public Application(String name, String appGroup, String appType, String description, int appCost, Date lastModified) {
    }

    public Application() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAppGroup() {
        return appGroup;
    }

    public void setAppGroup(String appGroup) {
        this.appGroup = appGroup;
    }

    public String getAppType() {
        return appType;
    }

    public void setAppType(String appType) {
        this.appType = appType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getAppCost() {
        return appCost;
    }

    public void setAppCost(long appCost) {
        this.appCost = appCost;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    @Override
    public String toString() {
        return "Application{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", appGroup='" + appGroup + '\'' +
                ", appType='" + appType + '\'' +
                ", description='" + description + '\'' +
                ", appCost=" + appCost +
                ", lastModified=" + lastModified +
                '}';
    }
}
