package com.homework.rmit;

import com.homework.rmit.model.Application;
import com.homework.rmit.repository.ApplicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.Instant;
import java.util.Date;

@SpringBootApplication

public class RmitApplication implements CommandLineRunner {
    private final ApplicationRepository applicationRepository;


    @Autowired
    public RmitApplication(ApplicationRepository applicationRepository) {
        this.applicationRepository = applicationRepository;
    }

    public static void main(String[] args){
        SpringApplication.run(RmitApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {

        Application application = new Application();
        application.setName("Project RMIT");
        application.setAppGroup("Back-End");
        application.setAppType("Java");
        application.setDescription("Java is programming language");
        application.setAppCost(500);
        application.setLastModified(Date.from(Instant.now()));
        System.out.println(applicationRepository.save(application));
    }
}
