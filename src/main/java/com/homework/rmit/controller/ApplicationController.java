package com.homework.rmit.controller;

import com.homework.rmit.model.Application;
import com.homework.rmit.repository.ApplicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class ApplicationController {
    private final ApplicationRepository applicationRepository;


    @Autowired
    public ApplicationController(ApplicationRepository applicationRepository) {
        this.applicationRepository = applicationRepository;
    }

    @GetMapping("/addApplication")
    public String showNewAppForm(Application application) {
        return "add-application";
    }

    @PostMapping("/add-application")
    public String addNewApp(@Valid Application application, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-application";
        }
        applicationRepository.save(application);
        model.addAttribute("apps", applicationRepository.findAll());
        return "/index";
    }

    @GetMapping("/delete/{id}")
    public String deleteApplication(@PathVariable("id") long id, Model model) {
        Application application = applicationRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid application"));
        applicationRepository.delete(application);
        model.addAttribute("apps", applicationRepository.findAll());
        return "/index";
    }


    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        Application application = applicationRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid Application"g));
        model.addAttribute("app", application);
        return "update-application";
    }

    @PostMapping("/update/{id}")
    public String updateApplication(@PathVariable("id") long id, @Valid Application application, BindingResult result, Model model) {
        if (result.hasErrors()) {
            application.setId(id);
            return "update-application";
        }
        applicationRepository.save(application);
        model.addAttribute("apps", applicationRepository.findAll());
        return "/index";
    }
}


